import abc

import orjson

from starlette import status
from starlette.endpoints import HTTPEndpoint
from starlette.background import BackgroundTasks
from starlette.requests import Request
from starlette.responses import Response

from tartiflette.engine import Engine

from graphene import Schema

from app.response import ORJSONResponse


class GraphqlEndpoint(HTTPEndpoint):
    @abc.abstractmethod
    async def _get_response(self, request: Request, data: dict) -> Response:
        ...

    async def post(self, request: Request) -> Response:
        content_type = request.headers.get("Content-Type", "")

        if "application/json" in content_type:
            try:
                body = await request.body()
                data = orjson.loads(body)
            except orjson.JSONDecodeError:
                return ORJSONResponse(
                    {"error": "Invalid JSON."},
                    status_code=status.HTTP_400_BAD_REQUEST,
                )
        else:
            return ORJSONResponse(
                {"error": "Unsupported Media Type"},
                status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            )

        return await self._get_response(request, data)


class TartifletteEndpoint(GraphqlEndpoint):
    async def _get_response(self, request: Request, data: dict) -> Response:
        try:
            query = data["query"]
        except KeyError:
            return ORJSONResponse(
                {"error": "No GraphQL query found in the request"},
                status_code=status.HTTP_400_BAD_REQUEST,
            )

        background = BackgroundTasks()
        context = {"request": request, "background": background}

        ttftt_engine: Engine = request.scope["graphql"]["ttftt_engine"]

        result = await ttftt_engine.execute(
            query,
            context=context,
            variables=data.get("variables"),
            operation_name=data.get("operationName"),
        )
        has_errors = "errors" in result
        status_code = (
            status.HTTP_400_BAD_REQUEST if has_errors else status.HTTP_200_OK
        )

        return ORJSONResponse(
            result, status_code=status_code, background=background
        )


class GrapheneEndpoint(GraphqlEndpoint):
    async def _get_response(self, request: Request, data: dict) -> Response:
        try:
            query = data["query"]
        except KeyError:
            return ORJSONResponse(
                {"error": "No GraphQL query found in the request"},
                status_code=status.HTTP_400_BAD_REQUEST,
            )

        background = BackgroundTasks()
        context = {"request": request, "background": background}
        graphene_schema: Schema = request.scope["graphql"]["graphene_schema"]

        result = await graphene_schema.execute(
            query,
            context=context,
            variables=data.get("variables"),
            operation_name=data.get("operationName"),
            return_promise=True,
        )
        has_errors = result.errors is not None
        status_code = (
            status.HTTP_400_BAD_REQUEST if has_errors else status.HTTP_200_OK
        )

        return ORJSONResponse(
            {"data": result.data, "errors": result.errors},
            status_code=status_code,
            background=background,
        )
