from starlette.applications import Starlette
from starlette.routing import Route
from starlette.middleware import Middleware

from tartiflette.engine import Engine

from app.endpoint import TartifletteEndpoint, GrapheneEndpoint
from app.middleware import GraphQLMiddleware

from starlette.responses import PlainTextResponse

import graphene


# Graphene
class Query(graphene.ObjectType):
    hello = graphene.String(required=True)

    def resolve_hello(self, info: graphene.ResolveInfo) -> str:
        return "Hello World!"


graphene_schema = graphene.Schema(query=Query)

# Tartiflette
ttftt_engine = Engine(
    sdl="app/graphql", modules=["app.resolvers.query_resolvers"]
)


# REST
async def root(request):
    return PlainTextResponse("Hello World")


app = Starlette(
    routes=[
        Route("/tartiflette", TartifletteEndpoint),
        Route("/graphene", GrapheneEndpoint),
        Route("/", root),
    ],
    on_startup=[ttftt_engine.cook],
    middleware=[
        Middleware(
            GraphQLMiddleware,
            ttftt_engine=ttftt_engine,
            graphene_schema=graphene_schema,
        )
    ],
)
