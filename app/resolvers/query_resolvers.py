from tartiflette import Resolver


@Resolver("Query.hello")
async def hello(parent, args, context, info):
    return "Hello World !"
