from starlette.types import ASGIApp, Scope, Receive, Send
from tartiflette.engine import Engine
from graphene import Schema


class GraphQLMiddleware:
    def __init__(
        self, app: ASGIApp, ttftt_engine: Engine, graphene_schema: Schema
    ):
        self.app = app
        self.ttftt_engine = ttftt_engine
        self.graphene_schema = graphene_schema

    async def __call__(
        self, scope: Scope, receive: Receive, send: Send
    ) -> None:
        scope["graphql"] = {
            "ttftt_engine": self.ttftt_engine,
            "graphene_schema": self.graphene_schema,
        }
        await self.app(scope, receive, send)
