# Results

## REST

```
./bombardier -c 1000 -n 1000000 -l -m GET http://localhost:8000/
```

<pre>
Bombarding http://localhost:8000/ with 1000000 request(s) using 1000 connection(s)
 1000000 / 1000000 [===============================================================] 100.00% 48960/s 20s
Done!
Statistics        Avg      Stdev        Max
  Reqs/sec     49370.64    8840.75   77294.58
  Latency       20.35ms     5.70ms   242.95ms
  Latency Distribution
     50%    19.89ms
     75%    22.50ms
     90%    26.11ms
     95%    27.66ms
     99%    38.83ms
  HTTP codes:
    1xx - 0, 2xx - 1000000, 3xx - 0, 4xx - 0, 5xx - 0
    others - 0
  Throughput:     9.69MB/s
</pre>

## Graphene

```
./bombardier -c 1000 -n 1000000 -l -m POST -H "Content-Type: application/json" -b '{ "query": "{ hello }"}' http://localhost:8000/graphene
```

<pre>
Bombarding http://localhost:8000/graphene with 1000000 request(s) using 1000 connection(s)
 1000000 / 1000000 [==============================================================] 100.00% 7488/s 2m13s
Done!
Statistics        Avg      Stdev        Max
  Reqs/sec      7505.16    7094.87   47255.51
  Latency      133.26ms    31.46ms   666.22ms
  Latency Distribution
     50%   127.72ms
     75%   176.75ms
     90%   212.04ms
     95%   221.69ms
     99%   286.66ms
  HTTP codes:
    1xx - 0, 2xx - 1000000, 3xx - 0, 4xx - 0, 5xx - 0
    others - 0
  Throughput:     2.27MB/s
</pre>

## Tartiflette

```
./bombardier -c 1000 -n 1000000 -l -m POST -H "Content-Type: application/json" -b '{ "query": "{ hello }"}' http://localhost:8000/tartiflette
```

<pre>
Bombarding http://localhost:8000/tartiflette with 1000000 request(s) using 1000 connection(s)
 1000000 / 1000000 [===============================================================] 100.00% 18095/s 55s
Done!
Statistics        Avg      Stdev        Max
  Reqs/sec     18187.11    6606.26   41876.02
  Latency       55.07ms    10.17ms   354.55ms
  Latency Distribution
     50%    49.01ms
     75%    63.66ms
     90%    74.69ms
     95%    84.99ms
     99%    97.44ms
  HTTP codes:
    1xx - 0, 2xx - 1000000, 3xx - 0, 4xx - 0, 5xx - 0
    others - 0
  Throughput:     5.33MB/s
</pre>
